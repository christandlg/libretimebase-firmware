### What is this repository for? ###

Firmware for the [LibreTimeBase](https://bitbucket.org/christandlg/libretimebase) Project. 
Currently, the Firmware supports LibreTimeBase Boards V 0.1.0 only, which have issues and are not ready for production. 

### How do I get set up? ###

* Install VS Code
* Install Platformio
* check out repository
* in VS Code, open 'src' folder
* run PlatformIO Build 
