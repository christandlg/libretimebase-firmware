/* 
LibreTimeBase ltb_efc.c
Copyright (C) 2021 Gregor Christandl

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software Foundation,
Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

#ifndef LTB_ERROR_H
#define LTB_ERROR_H

#include <stdbool.h>
#include <stdint.h>

enum _LtbErrorCode_ 
{
	LTB_ERROR_NONE = 0,
	
};

typedef enum _LtbErrorCode_ LtbErrorCode;

struct _LtbError_ 
{
	LtbErrorCode code_;
	
};
typedef struct _LtbError_ LtbError;
LtbError ltb_errors[32];

bool ltbErrorPush(LtbError error);
LtbError ltbErrorPop();

#endif /* LTB_ERROR_H */
