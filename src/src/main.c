/* 
LibreTimeBase main.c
Copyright (C) 2021 Gregor Christandl

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software Foundation,
Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

// #include <string>
#include "main.h"

#include "usart.h"

#include "usb_device.h"

// #include "adc.h"
// #include "crc.h"
// #include "dac.h"
// #include "dma.h"
// #include "i2c.h"
// #include "spi.h"
#include "tim.h"
// #include "gpio.h"

#include "scpi/scpi.h"
#include "../src/fifo_private.h"

#include "ltb_clock.h"
#include "ltb_led.h"
#include "ltb_panic.h"

void SystemClock_Config(void)
{
  //call default clock setup function
  ltbClockDefault();
}

void Error_Handler(void)
{
  panic();
}

int main(void)
{
  //initialization
  HAL_Init();

  SystemClock_Config();

  // SystemClock_Config();

  // MX_GPIO_Init();

  MX_USART1_UART_Init();
  MX_USB_DEVICE_Init();

  // MX_GPIO_Init();
  // MX_DMA_Init();
  // MX_USART2_UART_Init();
  // MX_ADC_Init();
  // MX_DAC_Init();
  // MX_I2C1_Init();
  // MX_SPI1_Init();
  // MX_SPI2_Init();
  // MX_TIM2_Init();
  // MX_TIM6_Init();
  // MX_TIM7_Init();
  // MX_TIM17_Init();
  // MX_USART4_UART_Init();
  // MX_CRC_Init();
  // MX_TIM1_Init();

  // char *testDataToSend = "hello world\n";

  double pwm_value = 0.0;
  double step = 0.2;

  char msg[64];

  ltbLedInit();

  while (1)
  {
    HAL_Delay(1000);

    // CDC_Transmit_FS(testDataToSend, 12);
    int length = sprintf(msg, "%lu\n", TIM3->CCR1);
    CDC_Transmit_FS(msg, length);

    //wait for tx to finish
    // while (((USBD_CDC_HandleTypeDef*)(USBD_Device.pClassData))->TxState != 0) 
    // ;
    // HAL_Delay(0);
    // CDC_Transmit_FS("12312312312\n", 12);


    // pwm_value = TIM3->CCR1;
    if(TIM3->CCR1 == 0) step = 0.2;
    if(TIM3->CCR1 > 60000) step = -0.2;
    pwm_value += step;

    ltbLedSetBrightness(LTB_LED_FAULT, pwm_value);
    ltbLedSetBrightness(LTB_LED_LOCK, pwm_value / 3);
    ltbLedSetBrightness(LTB_LED_GNSS, pwm_value);
    ltbLedSetBrightness(LTB_LED_RFU, pwm_value);


    //todo check supply voltage

    //todo check supply current

    //todo check temperatures

    //todo check BITE status

    //todo ext 1pps handling

    //todo clock source switch detection

    //todo DIB comms

    //todo RFS comms

    //todo USB comms

    //todo LED update

    //todo check RFS analog feedback
  }
}
