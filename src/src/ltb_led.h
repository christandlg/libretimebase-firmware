/* 
LibreTimeBase ltb_led.h
Copyright (C) 2021 Gregor Christandl

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software Foundation,
Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

#ifndef LTB_LED_H
#define LTB_LED_H

#include <stdbool.h>
#include <math.h>

#include "tim.h"

enum _LtbLed_ {LTB_LED_FAULT, LTB_LED_LOCK, LTB_LED_GNSS, LTB_LED_RFU};
typedef enum _LtbLed_ LtbLed;

bool ltbLedInit(void);

bool ltbLedSetBrightness(LtbLed led, double brightness);

double ltbLedGetBrightness(LtbLed led);

void ltbLedEnable(LtbLed led);

void ltbLedDisable(LtbLed led);

void ltbLedToggle(LtbLed led);

#endif /* LTB_LED_H */
