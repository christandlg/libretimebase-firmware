/* 
LibreTimeBase ltb_efc.c
Copyright (C) 2021 Gregor Christandl

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software Foundation,
Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

#include "ltb_error.h"

static uint8_t ltb_num_errors = 0;

bool ltbErrorPush(LtbError error)
{
	if (ltb_num_errors < sizeof(ltb_errors) / sizeof(ltb_errors[0]))
	{
		ltb_errors[ltb_num_errors] = error;
		ltb_num_errors++;
		
		return true;
	}
	
	return false;
}

LtbError ltbErrorPop()
{
	if (ltb_num_errors > 0)
	{
		ltb_num_errors--;
		return ltb_errors[ltb_num_errors+1];
	}
	
	LtbError error = {LTB_ERROR_NONE};
	return error;
}
