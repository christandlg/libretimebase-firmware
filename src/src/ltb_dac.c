/* 
LibreTimeBase ltb_dac.c
Copyright (C) 2021 Gregor Christandl

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software Foundation,
Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

#include "ltb_dac.h"

//---------------------------------------------------------------------- 
// initializes the SPI Handle and tests the connection.
bool ltbDacInitialize()
{
  DAC_ChannelConfTypeDef sConfig_ch1 = {0};
  DAC_ChannelConfTypeDef sConfig_ch2 = {0};
  
  hdac.Instance = DAC;
  if (HAL_DAC_Init(&hdac) != HAL_OK)
    return false;
    
  sConfig_ch1.DAC_Trigger = DAC_TRIGGER_T6_TRGO;
  sConfig_ch1.DAC_OutputBuffer = DAC_OUTPUTBUFFER_ENABLE;
  sConfig_ch2.DAC_Trigger = DAC_TRIGGER_T7_TRGO;
  sConfig_ch2.DAC_OutputBuffer = DAC_OUTPUTBUFFER_ENABLE;

  return 
    (HAL_DAC_ConfigChannel(&hdac, &sConfig_ch1, DAC_CHANNEL_1) != HAL_OK) &&
    (HAL_DAC_ConfigChannel(&hdac, &sConfig_ch2, DAC_CHANNEL_2) != HAL_OK);
}

//---------------------------------------------------------------------- 
// initializes the SPI Handle and tests the connection.
bool ltbDacEnable(uint32_t channel)
{
    return HAL_DAC_Start(&hdac, channel) == HAL_OK;
}

//---------------------------------------------------------------------- 
// initializes the SPI Handle and tests the connection.
bool ltbDacDisable(uint32_t channel)
{
    return HAL_DAC_Stop(&hdac, channel) == HAL_OK;
}

//---------------------------------------------------------------------- 
// initializes the SPI Handle and tests the connection.
uint16_t ltbDacGetValue(uint32_t channel)
{
    return (uint16_t)HAL_DAC_GetValue(&hdac, channel);
}

//---------------------------------------------------------------------- 
// initializes the SPI Handle and tests the connection.
bool ltbDacSetValue(uint32_t channel, uint16_t value)
{
    return 
        (HAL_DAC_SetValue(&hdac, channel, DAC_ALIGN_12B_L, (uint32_t) value) == HAL_OK) &&
        (HAL_DAC_Start(&hdac, channel) == HAL_OK);
}

//---------------------------------------------------------------------- 
// initializes the SPI Handle and tests the connection.
bool ltbDacSetSequence(uint32_t channel, uint16_t *data, uint16_t lenght, uint32_t sample_rate, bool repeat)
{
    //TODO DMA setup
}
