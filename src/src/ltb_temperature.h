/* 
LibreTimeBase ltb_temperature.h
Copyright (C) 2021 Gregor Christandl

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software Foundation,
Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

#ifndef LTB_TEMPERATURE
#define LTB_TEMPERATURE

#include <stdbool.h>
#include <stdint.h>
#include <string.h>
#include <math.h>

#include "main.h"

extern ADC_HandleTypeDef hadc;

enum _LTBTemperatureSensor_ 
{
    LTB_TEMPERATURE_SENSOR_T0 = 0,
    LTB_TEMPERATURE_SENSOR_T1 = 1,
    LTB_TEMPERATURE_SENSOR_T2 = 2,
    LTB_TEMPERATURE_SENSOR_uC = 3
};
typedef enum _LTBTemperatureSensor_ LTBTemperatureSensor;

bool ltbTemperatureInitialize();
double ltbGetTemperature(LTBTemperatureSensor sensor);
static double ltbTemperatureConvertMCP9700(uint32_t value);
static double ltbTemperatureConvertInternal(uint32_t value);

#endif /* LTB_TEMPERATURE */
