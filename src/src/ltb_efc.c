/* 
LibreTimeBase ltb_efc.c
Copyright (C) 2021 Gregor Christandl

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software Foundation,
Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

#include "ltb_efc.h"

static double ltb_efc_control_value_ = 0.0;

//---------------------------------------------------------------------- 
// initializes the SPI Handle and tests the connection.
// @return true on success, false otherwise. 
bool ltbEfcInitialize()
{
    hspi1.Instance = SPI1;
    hspi1.Init.Mode = SPI_MODE_MASTER;
    hspi1.Init.Direction = SPI_DIRECTION_2LINES;
    hspi1.Init.DataSize = SPI_DATASIZE_4BIT;
    hspi1.Init.CLKPolarity = SPI_POLARITY_LOW;
    hspi1.Init.CLKPhase = SPI_PHASE_1EDGE;
    hspi1.Init.NSS = SPI_NSS_HARD_OUTPUT;
    hspi1.Init.BaudRatePrescaler = SPI_BAUDRATEPRESCALER_16;
    hspi1.Init.FirstBit = SPI_FIRSTBIT_MSB;
    hspi1.Init.TIMode = SPI_TIMODE_DISABLE;
    hspi1.Init.CRCCalculation = SPI_CRCCALCULATION_DISABLE;
    hspi1.Init.CRCPolynomial = 7;
    hspi1.Init.CRCLength = SPI_CRC_LENGTH_DATASIZE;
    hspi1.Init.NSSPMode = SPI_NSS_PULSE_ENABLE;

    if (HAL_SPI_Init(&hspi1) != HAL_OK)
        return false;

    double val = ltbEfcGetValue();
    
    if (isnan(val))
        return false;

    return true;
}

//---------------------------------------------------------------------- 
// enables EFC.
// @return true on success, false otherwise. 
bool ltbEfcEnable()
{
    uint8_t tx[3] = {0x40, 0x00, 0xFC};     //DAC B disabled, DAC A enabled

    return HAL_SPI_Transmit(&hspi1, tx, 3, 1000) == HAL_OK;
}

//---------------------------------------------------------------------- 
// disables EFC.
// @return true on success, false otherwise. 
bool ltbEfcDisable()
{
    uint8_t tx[3] = {0x40, 0x00, 0xFF};     //DAC B and A disabled

    return HAL_SPI_Transmit(&hspi1, tx, 3, 1000) == HAL_OK;
}

//---------------------------------------------------------------------- 
// reads and returns the currently set EFC value. 
// @return currently set EFC value (-1.0 ... 1.0) on success, NAN otherwise. 
double ltbEfcGetValue()
{
    uint8_t tx[6];
    uint8_t rx[6];
    memset(tx, 0, 6);
    memset(rx, 0, 6);
    tx[0] = 0x90;

    if (HAL_SPI_TransmitReceive(&hspi1, tx, rx, 2, 1000) != HAL_OK)
        return NAN;

    uint16_t val = 0;
    memcpy(&val, rx+4, 2);

    return ((double)val / 32767.0) - 1.0;

    return ltb_efc_control_value_;
}

//---------------------------------------------------------------------- 
// set the EFC value. 
// @param value value to set (-1.0 ... 1.0)
// @return true on success, false otherwise. 
bool ltbEfcSetValue(double value)
{        
    if ((value < -1.0) || (value > 1.0))
        return false;

    uint16_t val = (uint16_t)((value + 1.0) * 32767.0);
    
    uint8_t tx[3];
    tx[0] = 0x31;   //Write to and update DAC Channel A
    memcpy(tx+1, &val, 2);
    
    if (HAL_SPI_Transmit(&hspi1, tx, 3, 1000) != HAL_OK)
        return false;

    ltb_efc_control_value_ = value;
    return true;
}
