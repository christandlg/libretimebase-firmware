/* 
LibreTimeBase ltb_panic.c
Copyright (C) 2021 Gregor Christandl

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software Foundation,
Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

#include "ltb_panic.h"

//---------------------------------------------------------------------- 
// attempts to set the device into a safe state and halts operation by calling panicHalt(). 
void panic()
{
    __disable_irq();

    //TODO set device into safe state. 

    panicHalt();
}

//---------------------------------------------------------------------- 
// indicates halted operation after panic by blinking the FAULT LED in a "siren" pattern
// code adapted from mbed's mbed_die() function.
// this function can only be exited by a reset / power cycle. 
void panicHalt()
{
    while (1)
    {
        //mbed siren LED

        //LED_FAULT_Pin
        //LED_FAULT_GPIO_Port
    }
}