/* 
LibreTimeBase ltb_dac.c
Copyright (C) 2021 Gregor Christandl

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software Foundation,
Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

#ifndef LTB_DAC
#define LTB_DAC

#include <stdbool.h>
#include <stdint.h>

#include "main.h"

uint16_t *ltb_dac_ch0_sequence_;
uint16_t *ltb_dac_ch1_sequence_;
uint32_t ltb_dac_ch1_sequence_length_;
uint32_t ltb_dac_ch1_sequence_length_;

extern DAC_HandleTypeDef hdac;

bool ltbDacInitialize();
bool ltbDacEnable(uint32_t channel);
bool ltbDacDisable(uint32_t channel);
uint16_t ltbDacGetValue(uint32_t channel);
bool ltbDacSetValue(uint32_t channel, uint16_t value);
bool ltbDacSetSequence(uint32_t channel, uint16_t *data, uint16_t lenght, uint32_t sample_rate, bool repeat);



#endif /* LTB_DAC */
