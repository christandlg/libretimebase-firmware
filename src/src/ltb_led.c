/* 
LibreTimeBase ltb_led.c
Copyright (C) 2021 Gregor Christandl

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software Foundation,
Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

#include "ltb_led.h"

static uint32_t ltb_led_fault_brigthness_ = 0;
static uint32_t ltb_led_lock_brigthness_ = 0;
static uint32_t ltb_led_GNSS_brigthness_ = 0;
static uint32_t ltb_led_RFU_brigthness_ = 0;

//−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−-------
// initializes Timer 3 for status LEDs
// @return true on success, false otherwise. 
bool ltbLedInit(void)
{
    MX_TIM3_Init();

    TIM_OC_InitTypeDef sConfigOC = {0};
    sConfigOC.OCMode = TIM_OCMODE_PWM1;
    sConfigOC.Pulse = 0x0000;
    sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
    sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;

    if (
        (HAL_TIM_PWM_Init(&htim3) != HAL_OK) ||
        (HAL_TIM_PWM_ConfigChannel(&htim3, &sConfigOC, TIM_CHANNEL_1) != HAL_OK) ||
        (HAL_TIM_PWM_ConfigChannel(&htim3, &sConfigOC, TIM_CHANNEL_2) != HAL_OK) ||
        (HAL_TIM_PWM_ConfigChannel(&htim3, &sConfigOC, TIM_CHANNEL_3) != HAL_OK) || 
        (HAL_TIM_PWM_ConfigChannel(&htim3, &sConfigOC, TIM_CHANNEL_4) != HAL_OK) ||
        (HAL_TIM_PWM_Start(&htim3, TIM_CHANNEL_1) != HAL_OK) ||
        (HAL_TIM_PWM_Start(&htim3, TIM_CHANNEL_2) != HAL_OK) ||
        (HAL_TIM_PWM_Start(&htim3, TIM_CHANNEL_3) != HAL_OK) ||
        (HAL_TIM_PWM_Start(&htim3, TIM_CHANNEL_4) != HAL_OK)
    )
        return false;

    return true;
}

//−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−-------
// sets the brightness of a specified LED. 
// @param led as LtbLed
// @param brightness (0.0 - 1.0)
// @return true on success, false otherwise. 
bool ltbLedSetBrightness(LtbLed led, double brightness)
{
    if ((brightness < 0.0) || (brightness > 1.0))
        return false;

    uint32_t pwm_val = TIM3->ARR;   //counter period
    pwm_val = (uint32_t)((double)pwm_val * brightness);

    switch(led)
    {
        case LTB_LED_FAULT:
            TIM3->CCR1 = pwm_val;
            ltb_led_fault_brigthness_ = pwm_val;
        break;
        case LTB_LED_LOCK:
            TIM3->CCR2 = pwm_val;
            ltb_led_lock_brigthness_ = pwm_val;
        break;
        case LTB_LED_GNSS:
            TIM3->CCR3 = pwm_val;
            ltb_led_GNSS_brigthness_ = pwm_val;
        break;
        case LTB_LED_RFU:
            TIM3->CCR4 = pwm_val;
            ltb_led_RFU_brigthness_ = pwm_val;
        break;
        default:
        break;
    }

    return true;
}

//−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−-------
// returns the currently set brigtness of a specified LED. 
// @param led as LtbLed
// @return LED brightness (0.0 - 1.0)
double ltbLedGetBrightness(LtbLed led)
{
    switch (led)
    {
    case LTB_LED_FAULT:
        return (double)(TIM3->CCR1) / (double)(TIM3->ARR);
    case LTB_LED_LOCK:
        return (double)(TIM3->CCR2) / (double)(TIM3->ARR);
    case LTB_LED_GNSS:
        return (double)(TIM3->CCR3) / (double)(TIM3->ARR);
    case LTB_LED_RFU:
        return (double)(TIM3->CCR4) / (double)(TIM3->ARR);
    }

    return NAN;
}

//−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−-------
// enables a specified LED using last set brightness for this LED. 
// @param led as LtbLed
void ltbLedEnable(LtbLed led)
{
    switch(led)
    {
        case LTB_LED_FAULT:
            if (TIM3->CCR1 == 0)
                TIM3->CCR1 = ltb_led_fault_brigthness_;
            break;
        case LTB_LED_LOCK:
            if (TIM3->CCR2 == 0)
                TIM3->CCR2 = ltb_led_lock_brigthness_;
            break;
        case LTB_LED_GNSS:
            if (TIM3->CCR3 == 0)
                TIM3->CCR3 = ltb_led_GNSS_brigthness_;
            break;
        case LTB_LED_RFU:
            if (TIM3->CCR4 == 0)
                TIM3->CCR4 = ltb_led_RFU_brigthness_;
            break;
    }
}

//−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−-------
// disables a specified LED. 
// @param led as LtbLed
void ltbLedDisable(LtbLed led)
{
    switch(led)
    {
        case LTB_LED_FAULT:
            TIM3->CCR1 = 0;
            break;
        case LTB_LED_LOCK:
            TIM3->CCR2 = 0;
            break;
        case LTB_LED_GNSS:
            TIM3->CCR3 = 0;
            break;
        case LTB_LED_RFU:
            TIM3->CCR4 = 0;
            break;
    }
}

//−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−−-------
// toggles a specified LED using last set brightness for this LED. 
// @param led as LtbLed
void ltbLedToggle(LtbLed led)
{
    switch(led)
    {
        case LTB_LED_FAULT:
            if (TIM3->CCR1 == 0) 
                TIM3->CCR1 = ltb_led_fault_brigthness_;
            else
                TIM3->CCR1 = 0;
            break;
        case LTB_LED_LOCK:
            if (TIM3->CCR2 == 0) 
                TIM3->CCR2 = ltb_led_lock_brigthness_;
            else
                TIM3->CCR2 = 0;
            break;
        case LTB_LED_GNSS:
            if (TIM3->CCR3 == 0) 
                TIM3->CCR3 = ltb_led_GNSS_brigthness_;
            else
                TIM3->CCR3 = 0;
            break;
        case LTB_LED_RFU:
            if (TIM3->CCR4 == 0) 
                TIM3->CCR4 = ltb_led_RFU_brigthness_;
            else
                TIM3->CCR4 = 0;
            break;
    }
}
