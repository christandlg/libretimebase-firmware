/* 
LibreTimeBase ltb_temperature.c
Copyright (C) 2021 Gregor Christandl

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software Foundation,
Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

#include "ltb_temperature.h"

static uint32_t ltb_temperature_ch10_val_ = 0;
static uint32_t ltb_temperature_ch11_val_ = 0;
static uint32_t ltb_temperature_ch12_val_ = 0;
static uint32_t ltb_temperature_ch16_val_ = 0;

bool ltbTemperatureInitialize()
{
    //ADC Setup
    ADC_ChannelConfTypeDef sConfig = {0};

    sConfig.Rank = ADC_RANK_CHANNEL_NUMBER;
    sConfig.SamplingTime = ADC_SAMPLETIME_13CYCLES_5;

    sConfig.Channel = ADC_CHANNEL_10;
    if (HAL_ADC_ConfigChannel(&hadc, &sConfig) != HAL_OK)
        return false;

    sConfig.Channel = ADC_CHANNEL_11;
    if (HAL_ADC_ConfigChannel(&hadc, &sConfig) != HAL_OK)
        return false;

    sConfig.Channel = ADC_CHANNEL_12;
    if (HAL_ADC_ConfigChannel(&hadc, &sConfig) != HAL_OK)
        return false;

    sConfig.Channel = ADC_CHANNEL_TEMPSENSOR;
    if (HAL_ADC_ConfigChannel(&hadc, &sConfig) != HAL_OK)
        return false;

    //DMA Setup
    //TODO
    

    return true;
}

double ltbTemperatureGet(LTBTemperatureSensor sensor)
{
    return NAN;
}

static double ltbTemperatureConvertMCP9700(uint32_t value)
{
    return ((double)value / 4096.0 * 3.3 - 0.5) / 0.01;
}

static double ltbTemperatureConvertInternal(uint32_t value)
{
    //datasheet 3.10.1 P18
    const uint16_t *ts_cal1 = (const uint16_t*)0x1FFFF7B8;
    const uint16_t *ts_cal2 = (const uint16_t*)0x1FFFF7C2;

    //RM0091 p251
    return (110.0 - 30.0) / ((double)(value - (uint32_t)*ts_cal1) / (double)((uint32_t)*ts_cal2 - (uint32_t)*ts_cal1)) + 30.0;    
}
