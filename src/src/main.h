/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under Ultimate Liberty license
  * SLA0044, the "License"; You may not use this file except in compliance with
  * the License. You may obtain a copy of the License at:
  *                             www.st.com/SLA0044
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32f0xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define B1_Pin GPIO_PIN_13
#define B1_GPIO_Port GPIOC
#define DIB_NFAULT_Pin GPIO_PIN_14
#define DIB_NFAULT_GPIO_Port GPIOC
#define DIB_SYNC_Pin GPIO_PIN_15
#define DIB_SYNC_GPIO_Port GPIOC
#define T_SENSOR_0_Pin GPIO_PIN_0
#define T_SENSOR_0_GPIO_Port GPIOC
#define T_SENSOR_1_Pin GPIO_PIN_1
#define T_SENSOR_1_GPIO_Port GPIOC
#define T_SENSOR_2_Pin GPIO_PIN_2
#define T_SENSOR_2_GPIO_Port GPIOC
#define RFS_LOCK_Pin GPIO_PIN_3
#define RFS_LOCK_GPIO_Port GPIOC
#define uC_RFS_1PPS_in_Pin GPIO_PIN_0
#define uC_RFS_1PPS_in_GPIO_Port GPIOA
#define uC_EXT_1PPS_in_Pin GPIO_PIN_1
#define uC_EXT_1PPS_in_GPIO_Port GPIOA
#define RFS_UART_TX_Pin GPIO_PIN_2
#define RFS_UART_TX_GPIO_Port GPIOA
#define RFS_UART_RX_Pin GPIO_PIN_3
#define RFS_UART_RX_GPIO_Port GPIOA
#define RFS_VREF_0_Pin GPIO_PIN_6
#define RFS_VREF_0_GPIO_Port GPIOA
#define RFS_VREF_1_Pin GPIO_PIN_7
#define RFS_VREF_1_GPIO_Port GPIOA
#define I_VDDA_0_Pin GPIO_PIN_4
#define I_VDDA_0_GPIO_Port GPIOC
#define I_VDDA_1_Pin GPIO_PIN_5
#define I_VDDA_1_GPIO_Port GPIOC
#define RFS_PWR_en_Pin GPIO_PIN_2
#define RFS_PWR_en_GPIO_Port GPIOB
#define LED_FAULT_Pin GPIO_PIN_6
#define LED_FAULT_GPIO_Port GPIOC
#define LED_LOCK_Pin GPIO_PIN_7
#define LED_LOCK_GPIO_Port GPIOC
#define LED_GNSS_Pin GPIO_PIN_8
#define LED_GNSS_GPIO_Port GPIOC
#define LED_RFU_Pin GPIO_PIN_9
#define LED_RFU_GPIO_Port GPIOC
#define TMS_Pin GPIO_PIN_13
#define TMS_GPIO_Port GPIOA
#define TCK_Pin GPIO_PIN_14
#define TCK_GPIO_Port GPIOA
#define GNSS_UART_TX_Pin GPIO_PIN_10
#define GNSS_UART_TX_GPIO_Port GPIOC
#define GNSS_UART_RX_Pin GPIO_PIN_11
#define GNSS_UART_RX_GPIO_Port GPIOC
#define GNSS_Reset_Pin GPIO_PIN_12
#define GNSS_Reset_GPIO_Port GPIOC
#define OUT_SQW_EN_Pin GPIO_PIN_2
#define OUT_SQW_EN_GPIO_Port GPIOD
#define uC_1PPS_out_Pin GPIO_PIN_9
#define uC_1PPS_out_GPIO_Port GPIOB
/* USER CODE BEGIN Private defines */

/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
